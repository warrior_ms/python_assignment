# In python if we add 2 strings, they get concatenated.

print("I am " + "Mustafa Sadriwala")

# And if we multiply a string with an integer, it gets replicated that many times.

print("*" * 10)


# Output :
# I am Mustafa Sadriwala
# **********