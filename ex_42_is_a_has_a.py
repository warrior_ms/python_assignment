class Animal(object):
    def __str__(self):
        return 'Animal class'
    
class Dog(Animal):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return 'Dog class'
        
        
class Cat(Animal):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return 'Cat class'
        
class Person(object):
    def __init__(self, name):
        self.name = name
        self.pet = None

    def __str__(self):
        return 'Person class'
        
class Employee(Person):
    def __init__(self, name, salary):
        super(Employee, self).__init__(name)
        self.salary = salary

    def __str__(self):
        return 'Employee class'
        
class Fish(object):
    def __str__(self):
        return 'Fish class'
    
class Salmon(Fish):
    def __str__(self):
        return 'Salmon class'

    
rover = Dog("Rover")
satan = Cat("Satan")
mary = Person("Mary")
mary.pet = satan
frank = Employee("Frank", 120000)
frank.pet = rover
flipper = Fish()
crouse = Salmon()

print(rover, end="\n")
print(satan, end="\n")
print(mary, end="\n")
print(frank, end="\n")
print(flipper, end="\n")
print(crouse, end="\n")





# Output :
# Dog class
# Cat class
# Person class
# Employee class
# Fish class
# Salmon class