i = 1
squares = []

while i <= 10:
    squares.append(i ** 2)
    i = i + 1

print(squares)



# Output :
# [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]