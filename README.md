## Submitted By : Mustafa Sadriwala

This repository contains the codes and programs made in accordance of the book "Learn Python the Hard Way, 3rd Edition", to learn python.

Each file contained in this repo has code in it, corresponding to the excercises in the book, as well as the corresponding output of each program is given in commented manner.

[In case of some excercises having similar content, I have merged those excercises into a single file.]