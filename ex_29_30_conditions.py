# In python, there are conditional statements as well. They are used as follows :

# Syntax :  if <condition> :
#               <statement>
#
#           elif <condition> :
#               <statement>
#           else :
#               <statement>


var_1 = int(input("Enter number 1 :\n"))
var_2 = int(input("Enter number 2 :\n"))

if var_1 == var_2 :
    print("They are equal.")

elif var_1 > var_2 :
    print("{} is greater".format(var_1))

else :
    print("{} is greater".format(var_2))


# Output :
# Enter number 1 :
# 12
# Enter number 2 :
# 15
# 15 is greater