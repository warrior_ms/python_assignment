# 'formatter' is an interface which is used to write formatted output.

# The code given below is taken from the book, and it displays how exactly is 'formatter' implemented.

formatter = "%r %r %r %r"
print(formatter %(1, 2, 3, 4))
print(formatter %("one", "two", "three", "four"))
print(formatter % (True, False, False, True))
print(formatter % (formatter, formatter, formatter, formatter))
print(formatter % ("I had this thing.", "That you could type up right.", "But it didn't sing.", "So I said goodnight."))



# Output :
# 1 2 3 4
# 'one' 'two' 'three' 'four'
# True False False True
# '%r %r %r %r' '%r %r %r %r' '%r %r %r %r' '%r %r %r %r'
# 'I had this thing.' 'That you could type up right.' "But it didn't sing." 'So I said goodnight.'