# Functions can also return values. It is done by return keyword.

def return_rem(num, divisor):
    return num % divisor

print("The remainder is:\n" + str(return_rem(int(input("Enter number:\n")), int(input("Enter divisor:\n")))))



# Output :
# Enter number:
# 25
# Enter divisor:
# 12
# The remainder is:
# 1