# There are four collection data types in the Python programming language:
# 1. List is a collection which is ordered and changeable. Allows duplicate members.
# 2. Tuple is a collection which is ordered and unchangeable. Allows duplicate members.
# 3. Set is a collection which is unordered and unindexed. No duplicate members.
# 4. Dictionary is a collection which is unordered, changeable and indexed. No duplicate members.


# There are basically 2 types of looping statements in Python :
# 1. for loop
# 2. while loop


lst = [1,2,3,5,8.9,5.3,'a','abc',(1,2,3)]

for i in lst:
    print(i)



# Output :
# 1
# 2
# 3
# 5
# 8.9
# 5.3
# a
# abc
# (1, 2, 3)