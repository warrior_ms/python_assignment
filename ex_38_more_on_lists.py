ten_things = "Apples Oranges Crows Telephone Light Sugar"
print("Wait there's not 10 things in that list, let's fix that.")
stuff = ten_things.split(' ')
more_stuff = ["Day", "Night", "Song", "Frisbee", "Corn", "Banana", "Girl", "Boy"]
while len(stuff) != 10:
    next_one = more_stuff.pop()
    print("Adding: " + next_one)
    stuff.append(next_one)
    print("There's {} items now.".format(len(stuff)))

print("There we go: ", end = " ")
print(stuff)
print("Let's do some things with stuff.")
print("First element is :" + stuff[1])
print("Last element is :" + stuff[- 1])


# Output :
# Wait there's not 10 things in that list, let's fix that.
# Adding: Boy
# There's 7 items now.
# Adding: Girl
# There's 8 items now.
# Adding: Banana
# There's 9 items now.
# Adding: Corn
# There's 10 items now.
# There we go:  ['Apples', 'Oranges', 'Crows', 'Telephone', 'Light', 'Sugar', 'Boy', 'Girl', 'Banana', 'Corn']
# Let's do some things with stuff.
# First element is :Oranges
# Last element is :Corn