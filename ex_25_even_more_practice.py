def split_words(sentence):
    return sentence.split()

def join_words(word_1, word_2):
    return word_1 + word_2

def first_letter(data):
    return data[0]

def last_letter(data):
    return data[-1]

def reverse_sentence(sentence):
    return sentence[:: -1]





# Output :
# PS C:\Users\consultadd 1\Desktop\python_assignment> python
# Python 3.8.1 (tags/v3.8.1:1b293b6, Dec 18 2019, 22:39:24) [MSC v.1916 32 bit (Intel)] on win32
# Type "help", "copyright", "credits" or "license" for more information.
# >>> import ex_25_even_more_practice as module
# >>> module.split_words("Hello there...!!!")   
# ['Hello', 'there...!!!']
# >>> module.join_words("Hello"," there...!!!")
# 'Hello there...!!!'
# >>> module.first_letter("Hello")                
# 'H' 
# >>> module.last_letter("Hello")  
# 'o' 
# >>> module.reverse_sentence("Hello there...!!!") 
# '!!!...ereht olleH'
# >>>