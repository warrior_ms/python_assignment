print("Understanding comments")

# In python '# is used for placing comments, they are used by developers to make their code more readable and understandable.'
# '#' is a one line comment in python.

# There is also a concept of docstring in python, which is usually misunderstood as a multi-line comment, but it is exactly not a multi-line comment, but in some naive programming can be used so.

""" This is a docstring, it is used for documentation of a module, function, class or method in python. """

print("These comments and docstring is not executed by python interpreter, hence does not hinder programming logic.")




# Output :
# Understanding comments
# These comments and docstring is not executed by python interpreter, hence does not hinder programming logic.