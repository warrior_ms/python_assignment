#  list of basic commands related to file handling in python:
#  • f.close()      — Closes the ﬁ le. Like File-> Save.. in your editor.
#  • f.read()       — Reads the contents of the ﬁ le. You can assign the result to a variable.
#  • f.readline()   — Reads just one line of a text ﬁ le.
#  • f.truncate()   — Empties the ﬁ le. Watch out if you care about the ﬁ le.
#  • f.write(stuff) — Writes stuff to the ﬁ le.

# Note : f in the commands denote the filehandler


from sys import argv

script, filename = argv

data = input("Enter the data to be entered into the file :")

with open(filename, 'w') as f:
    print(f.write(data))

with open(filename, 'r') as f:
    print(f.read())



# Output :                                                              [Command : python ex_16_writing_files.py temp.txt]
# Enter the data to be entered into the file :I am Mustafa Sadriwala    [File created and data entered successfully]
# 22                                                                    # (Size of the data written, returned by write() function)
# I am Mustafa Sadriwala