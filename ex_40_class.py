# Python is an object oriented programming language.
# Almost everything in Python is an object, with its properties and methods.

# All classes have a function called __init__(), which is always executed when the class is being initiated.
# The __init__() function is used to assign values to object properties, or other operations that are necessary to do when the object is being created.

# The "self" parameter is a reference to the current instance of the class, and is used to access variables that belongs to the class.
# It does not have to be named self , you can call it whatever you like, but it has to be the first parameter of any function in the class.

class student:
    """ Student class, intended to store details of student and return the percentage. """

    def __init__ (self, name, marks):
        self.name = name
        self.marks = marks

    def percentage(self):
        return ((self.marks / 60) * 100)


student_obj = student(input("Enter name :\n"), int(input("Enter marks :\n")))
print("Percentage marks scored by {} is : {}".format(student_obj.name, student_obj.percentage()))



# Output :
# Enter name :
# Akash
# Enter marks :
# 50
# Percentage marks scored by Akash is : 83.33333333333334