# This is a simple example of prompting a user using python.

from sys import argv
script, user_name = argv    # Unpacking argument values.
prompt = '> '
print("Hi {}, I'm the {} script.".format(user_name, script))
print("I'd like to ask you a few questions.")
print("{} what is your age?".format(user_name))
likes = input(prompt)
print("Where do you live {}?".format(user_name))
lives = input(prompt)
print(""" Alright, so you said {} is your age.\n And you live in {}.""".format(likes, lives))



# Output :   [Command : python ex_14_prompting.py Mustafa]
# Hi Mustafa, I'm the ex_14_prompting.py script.
# I'd like to ask you a few questions.
# Mustafa what is your age?
# > 21
# Where do you live Mustafa?
# > Indore
#  Alright, so you said 21 is your age.
#  And you live in Indore.