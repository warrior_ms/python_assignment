# In python, inputs are taken into program using 'input()' function.

# Note : 'input()' always returns the input in 'string' format, so if the data is needed into some other data-type, it should be type-casted accordingly.

print("Hello! " + input("Enter your name\n"))

print(type(input("Enter any number : \n")))



# Output :
# Enter your name
# Mustafa
# Hello! Mustafa
# Enter any number :
# 25
# <class 'str'>