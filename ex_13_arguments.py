# The parameters which we pass to the program while running it's execution command (like : python <filename>.py  <arg1> <arg2> ... <argn>), 
# are termed as run time parameters, or arguments.

# In python, we can deal with them, using 'sys' module.

from sys import argv

script, first_param, second_param = argv        # Unpacking argument values.

print("Hello {}, you mentioned your age as : {}".format(first_param, second_param))

# In this example, as we can see in line 8, the content of 'argv', got assigned to the variables automatically.
# This technique in which the the different values of a variable gets assigned to comma-separated variables, is known as unpacking of variables, and vice-versa is known as packing of variables.



# Output : [command was : python ex_13_arguments.py Mustafa 21 ]
# Hello Mustafa, you mentioned your age as : 21