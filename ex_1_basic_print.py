print("Hello World!")
print("Printing number : ", end = "  :  ")
print(25)
print("Printing string : ", end = "  :  ")
print("Mustafa")
print("Printing list : ", end = "  :  ")
print([1,2.3,4,"a"])
print("Printing set : ", end = "  :  ")
print({1,2,5,6})
print("Printing dictionary : ", end = "  :  ")
print(dict({1: "a", 2: "b"}))
print("Printing tuple : ", end = "  :  ")
print((1,5,9,7,3))




# Output :
# Hello World!
# Printing number :   :  25
# Printing string :   :  Mustafa
# Printing list :   :  [1, 2.3, 4, 'a']
# Printing set :   :  {1, 2, 5, 6}
# Printing dictionary :   :  {1: 'a', 2: 'b'}
# Printing tuple :   :  (1, 5, 9, 7, 3)