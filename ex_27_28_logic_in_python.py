# There are basically 3 logical operators in python :

# "and" : Logical AND :
#     If both the operands are true then condition becomes true.

# "or" : Logical OR :
#     If any of the two operands are true then condition becomes true.

# "not" : Logical NOT :
#     Used to reverse the logical state of its operand.


print(True and True)
print(True and False)
print(False and False)
print(True or True)
print(True or False)
print(False or False)
print(1 == 1 and 2 == 1)
print(1 == 1 or 2 == 1)
print("test" == "test")
print(True and 1 == 1)
print(True or 1 != 1)
print(not(True and 1 == 1))
print(not (1 == 1 and 0 != 1))
print( "chunky" == "bacon" and not (3 == 4 or 3 == 3))



# Output :
# True
# False
# False
# True
# True
# False
# False
# True
# True
# True
# True
# False
# False
# False