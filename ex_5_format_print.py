#  We can embed variables inside a string by using specialized format sequences in python, and then put those variables at the time of execution.
# In python 3, it is achieved by using 'format()' function. 

details = input("Enter your name and age : \n").split()   # Get name and age of user in a list named 'deatils'.

print("Hello {}, the age you entered is : {}".format(details[0], int(details[1])))



# Output :
# Enter your name and age :
# Mustafa 21
# Hello Mustafa, the age you entered is : 21