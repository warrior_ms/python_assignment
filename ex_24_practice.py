print("Let's practice everything.")
print("You\'d need to know \'bout escapes with \\ that do \n newlines and \t tabs.")
poem = """\tThe lovely world with logic so firmly planted cannot discern\nthe needs of love nor comprehend passion from intuition and requires an explanation\n\t\twhere there is none."""
print("---------x----------")
print(poem)
print("---------x----------")
res = 10 - 2 + 3 - 6
print("The result is : {}".format(res))

def func(num):
    return num ** 2, num / 10 

num = int(input("Enter number : \n"))
res1, res2 = func(num)
print("Square is :\n{}\nDivision by 10 gives :\n{}".format(res1, res2))




# Output :
# Let's practice everything.
# You'd need to know 'bout escapes with \ that do
#  newlines and    tabs.
# ---------x----------
#         The lovely world with logic so firmly planted cannot discern
# the needs of love nor comprehend passion from intuition and requires an explanation
#                 where there is none.
# ---------x----------
# The result is : 5
# Enter number :
# 25
# Square is :
# 625
# Division by 10 gives :
# 2.5