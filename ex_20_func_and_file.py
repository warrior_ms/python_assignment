# In python we can pass filehandlers to user-defined functions, so that specific functions can also perform file related operations.

from sys import argv

def read_file(file_handler):
    print(file_handler.read())

def read_by_line(file_handler):
    print(file_handler.readlines())

script, filename = argv
with open(filename) as f:
    print("Data from file is :")
    read_file(f)
    
    f.seek(0)           # To reset file pointer to start of file
    
    print("\n\nData from file, line wise is :")
    read_by_line(f)




# Output :      [Command : python ex_20_func_and_file.py temp.txt]
# Data from file is :
# I am Mustafa Sadriwala
# I am Associate Software Engineer Intern


# Data from file, line wise is :
# ['I am Mustafa Sadriwala\n', 'I am Associate Software Engineer Intern']