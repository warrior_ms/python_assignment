# Here we will see basic mathematical functions and the way to use them in python.

var_a = 25
var_b = 20.2
print("Basic maths functions")
print("sum is               : " + str(var_a + var_b))
print("difference is        : " + str(var_a - var_b))
print("product is           : " + str(var_a * var_b))
print("division is          : " + str(var_a / var_b))
print("integer division is  : " + str(var_a // var_b))
print("remainder is         : " + str(var_a % var_b))

# These are basic mathematical functions and rest there are many other functions which are available in "Math" module in python. Some of those are as follows
import math

print("\nSome Functions from 'math' module")
print("Ceil is              : " + str(math.ceil(var_b)))
print("Floor is             : " + str(math.floor(var_b)))
print("Truncation is        : " + str(math.trunc(var_b)))
print("Exponentiation is    : " + str(math.pow(var_b, var_a)))
print("Sine value is        : " + str(math.sin(var_b)))




#Output :
# Basic maths functions
# sum is               : 45.2
# difference is        : 4.800000000000001
# product is           : 505.0
# division is          : 1.2376237623762376
# integer division is  : 1.0
# remainder is         : 4.800000000000001

# Some Functions from 'math' module
# Ceil is              : 21
# Floor is             : 20
# Truncation is        : 20
# Exponentiation is    : 4.3031277171430064e+32
# Sine value is        : 0.9758205177669755