# Function is nothing but a set of instructions that performs a specific task for the main routine or we can say, the main program.

# In python, functions are of mainly 3 types :
# 1. Built-in functions.
# 2. User-Defined Functions.
# 3. Anonymous functions.

def find_75_per(total_lec):
    return (total_lec * 3) >> 2

def find_attendence(total_lec, attended_lec):
    return (attended_lec / total_lec) * 100


total_lec = int(input("Enter the total lectures :\n"))
print("75 percent attendence would be at : " + str(find_75_per(total_lec)))     # passing variables to function

attended_lec = int(input("Enter the number of lectures you missed :\n"))
print("Your current attendence is : " + str(find_attendence(total_lec, (total_lec - attended_lec))))       # performing computation to pass as an argument to function


# Output :
# Enter the total lectures :
# 16
# 75 percent attendence would be at : 12
# Enter the number of lectures you missed :
# 5
# Your current attendence is : 68.75