states = {'Oregon': 'OR', 'Florida': 'FL', 'California': 'CA', 'New York': 'NY', 'Michigan': 'MI'}
cities = {'CA': 'San Francisco', 'MI': 'Detroit', 'FL': 'Jacksonville'}

cities['NY'] = 'New York'
cities['OR'] = 'Portland'

print('- ' * 10)
print("NY State has: " + cities['NY'])
print("OR State has: " + cities['OR'])
print("- " * 10)
print("Michigan's abbreviation is: " + states['Michigan'])
print("Florida's abbreviation is: " + states['Florida'])
print("- " * 10)
print("Michigan has: "+ cities[states['Michigan']])
print("Florida has: " + cities[states['Florida']])
print("- " * 10)

for state, abbrev in states.items():
    print("{} is abbreviated as {}".format(state, abbrev))
    
print("- " * 10)
for abbrev, city in cities.items():
    print("{} has the city {}".format(abbrev, city))




# Output :
# - - - - - - - - - -
# NY State has: New York
# OR State has: Portland
# - - - - - - - - - -
# Michigan's abbreviation is: MI
# Florida's abbreviation is: FL
# - - - - - - - - - -
# Michigan has: Detroit
# Florida has: Jacksonville
# - - - - - - - - - -
# Oregon is abbreviated as OR
# Florida is abbreviated as FL
# California is abbreviated as CA
# New York is abbreviated as NY
# Michigan is abbreviated as MI
# - - - - - - - - - -
# CA has the city San Francisco
# MI has the city Detroit
# FL has the city Jacksonville
# NY has the city New York
# OR has the city Portland