# There are certain escape sequences which can be used in 'print' statement in python.

# 1.  \    : Backslash and newline ignored
# 2.  \\   : Backslash (\)
# 3.  \n   : ASCII Linefeed (Newline in output)
# 4.  \'   : Single quote (')
# 5.  \"   : Double quote (")
# 6.  \a   : ASCII Bell (BEL)
# 7.  \f   : ASCII Formfeed (FF)
# 8.  \r   : ASCII Carriage Return (CR)
# 9.  \t   : ASCII Horizontal Tab (TAB)
# 10. \b   : ASCII Backspace (BS)
# 11. \v   : ASCII Vertical Tab (VT)
# 12. \ooo : Character with octal value 'ooo'
# 13. \xhh : Character with hex value 'hh'

print("I am \\Mustafa")
print("I am \nMustafa")
print("I am \'Mustafa\'")
print("I am \"Mustafa\"")
print("I am \aMustafa")
print("I am \fMustafa")
print("I am \rMustafa")
print("I am \tMustafa")
print("I am \vMustafa")
print("\144")
print("\x48")

# We can print docstrings as well, using print function

print(""" This is a docstring """)



# Output :
# I am \Mustafa
# I am
# Mustafa
# I am 'Mustafa'
# I am "Mustafa"
# I am Mustafa
# I am 
#      Mustafa
# Mustafa
# I am    Mustafa
# I am 
#      Mustafa
# d
# H
#  This is a docstring