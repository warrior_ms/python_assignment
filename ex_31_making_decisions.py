print("You enter a dark room with two doors.\nDo you go through door #1 or door #2?")
door = input("> ")
if door == "1":
    print("There's a giant bear here eating a cheese cake.\nWhat would you do?")
    print("1. Take the cake.")
    print("2. Scream at the bear.")
    decision = input("> ")
    if decision == "1":
        print("The bear eats your face off.")
    
    elif decision == "2":
        print("The bear eats your legs off.")
        
    else:
        print("Well, doing {} is probably better. Bear runs away.".format(decision))
    
elif door == "2":
    print("You stare into the endless abyss at Cthulhu's retina.")
    print("1. Blueberries.")
    print("2. Yellow jacket clothespins.")
    print("3. Understanding revolvers yelling melodies.")

    insanity = input("> ")
    if insanity == "1" or insanity == "2":
        print("Your body survives powered by a mind of jello.")

    else:
        print("The insanity rots your eyes into a pool of muck.")
            
else:
    print("You stumble around and fall on a knife and die.")




# Output :
# You enter a dark room with two doors.
# Do you go through door #1 or door #2?
# > 1
# There's a giant bear here eating a cheese cake.
# What would you do?
# 1. Take the cake.
# 2. Scream at the bear.
# > 1
# The bear eats your face off.