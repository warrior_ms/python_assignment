# Variables are nothing but reserved memory locations to store values.

# Rules for defining variable in python :
# 1. A variable is created the moment you first assign a value to it.
# 2. Variables do not need to be declared with any particular data-type and can even change type after they have been set. (Duck typing)
# 3. A variable name must start with a letter or the underscore character.
# 4. A variable name cannot start with a number
# 5. A variable name can only contain alpha-numeric characters and underscores (A-z, 0-9, and _ )
# 6. Variable names are case-sensitive (age, Age and AGE are three different variables)

# According to PEP8 specifications, '_' has a significance in naming variables in python. It states :
# 1. '_single_leading_underscore'                 : weak "internal use" indicator. E.g. from M import * does not import objects whose names start with an underscore.
# 2. 'single_trailing_underscore_'                : used by convention to avoid conflicts with Python keyword.
# 3. '__double_leading_underscore'                : when naming a class attribute, invokes name mangling (inside class FooBar, __boo becomes _FooBar__boo; see below).
# 4. '__double_leading_and_trailing_underscore__' : "magic" objects or attributes that live in user-controlled namespaces. E.g. '__init__' , '__import__' or '__file__'.

# example from book, converted to python 3
cars = 100
space_in_a_car = 4
drivers = 30
passengers = 90
cars_not_driven = cars - drivers
cars_driven = drivers
carpool_capacity = cars_driven * space_in_a_car
average_passengers_per_car = passengers / cars_driven
print("There are " + str(cars) + " cars available.")
print("There are only " + str(drivers) + " drivers available.")
print("There will be " + str(cars_not_driven) + " empty cars today.")
print("We can transport " + str(carpool_capacity) + " people today.")
print("We have " + str(passengers) + " to carpool today.")
print("We need to put about " + str(average_passengers_per_car) + " in each car.")




# output :
# There are 100 cars available.
# There are only 30 drivers available.
# There will be 70 empty cars today.
# We can transport 120 people today.
# We have 90 to carpool today.
# We need to put about 3.0 in each car.