# There are different file modes, which define in which manner, the file is to be used. They are:

# 'r' : Open a file for reading. (default)
# 'w' : Open a file for writing. Creates a new file if it does not exist or truncates the file if it exists.
# 'x' : Open a file for exclusive creation. If the file already exists, the operation fails.
# 'a' : Open for appending at the end of the file without truncating it. Creates a new file if it does not exist.
# 't' : Open in text mode. (default)
# 'b' : Open in binary mode.
# '+r' : Open a file for updating (reading and writing)


from sys import argv

script, file_to_copy, file_to_write = argv

with open(file_to_copy, 'r') as f:
    data = f.read()

with open(file_to_write, 'w') as f:
    f.write(data)


print("File copied")




# Output :            [Command : python ex_17_copying_file.py temp.txt new.txt]
# File copied         [File named "new.txt" succesfully created and data copying successful]