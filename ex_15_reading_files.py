# With python, we can also perform file handling.

from sys import argv

script, filename = argv

print("The data from the file is :")

with open(filename) as f:
    print(f.read())



# Output :   [Command : python ex_15_reading_files.py README.md ]
# The data from the file is :
# ## Submitted By : Mustafa Sadriwala

# This repo contains the codes and programs made in accordance of the book "Learn Python the Hard Way, 3rd Edition", to learn python.

# Each file contained in this repo has code in it,corresponding to the excercises in the book, as well as the corresponding output of each program is given in commented manner.

# [In case of some excercises having similar content, I have merged those excercises into a single file.]